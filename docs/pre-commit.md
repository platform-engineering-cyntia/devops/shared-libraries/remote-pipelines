<div align="center">
<h1>Pre Commit Hooks - Rules Enabled</h1>
</div>

<hr>

#### `check-added-large-files`

Impedir que arquivos gigantes sejam confirmados.

- Especifique o que é muito grande com (default=500kb). `args: ['--maxkb=123']`
- Limita os arquivos indicados como preparados para adição pelo git.
- Se estiver instalados, arquivos lfs serão ignorados (requer git-lfs, git-lfs>=2.2.1)
- `--enforce-all` - Verifique todos os arquivos listados, não apenas aqueles preparados para adição.


#### `check-yaml`

Tenta carregar todos os arquivos yaml yaml para verificar a sintaxe.

#### `trailing-whitespace`

Apara espaços em branco para à direita.

#### `end-of-file-fixer`

Garante que os arquivos terminem em uma nova linha e somente em uma nova linha.


<a href="../README.md">  <-- Voltar </a>
