#!/bin/bash

if [ -z ${SONAR_PROJECT_KEY+x} ]; then
  echo "Problema ao setar o nome do projeto."
  exit 1
fi

if [ -z ${SONAR_GATE_NAME+x} ]; then
  echo "O nome do quality gate não foi setado."
  exit 1 
fi

RESPONSE_CREATE_PROJECT=`curl -s\
  -X POST \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -u "${SONAR_TOKEN}": \
  -d "project=${SONAR_PROJECT_KEY}&name=${SONAR_PROJECT_KEY}" \
  "${SONAR_HOST_URL}/api/projects/create"`

echo $RESPONSE_CREATE_PROJECT;

CREATED_PROJECT=`echo $RESPONSE_CREATE_PROJECT | jq -r '.project.key'` || true
CREATE_ERRORS=`echo $RESPONSE_CREATE_PROJECT | jq -r '.errors'` || true

if [ "$CREATED_PROJECT" = "$SONAR_PROJECT_KEY" ]; then
  echo "Projeto ${SONAR_PROJECT_KEY} criado."

  RESPONSE_SET_TAGS=`curl -s\
    -o /dev/null \
    -w "%{http_code}\n" \
    -X POST \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -u "${SONAR_TOKEN}": \
    -d "project=${SONAR_PROJECT_KEY}&tags=${CI_PROJECT_NAME}" \
    "${SONAR_HOST_URL}/api/project_tags/set"`

  if [ $RESPONSE_SET_TAGS = 204 ]; then
    echo "Tags do projeto setadas com sucesso."
  else 
    echo "Erro ao setar tags, verifique."
  fi

elif [ -z "${CREATE_ERRORS##*Could not create Project, key already exists*}" ]; then
  echo "O projeto ${SONAR_PROJECT_KEY} já está criado."
elif [ -z "${CREATE_ERRORS##*Could not create Project with key*}" ]; then
  echo "O projeto ${SONAR_PROJECT_KEY} já está criado."
else
  echo "Erro ao criar projeto."
  exit 1
fi

RESPONSE_ASSOC_GATE=`curl -s \
  -o /dev/null \
  -w "%{http_code}\n" \
  -X POST \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -u "${SONAR_TOKEN}": \
  -d "gateName=${SONAR_GATE_NAME}&projectKey=${SONAR_PROJECT_KEY}" \
  "${SONAR_HOST_URL}/api/qualitygates/select"`
  
if [ $RESPONSE_ASSOC_GATE = 204 ]; then
  echo "${SONAR_GATE_NAME} setado como o quality gate do projeto ${SONAR_PROJECT_KEY}."
else 
  echo "Erro ao selecionar o quality gate."
  exit 1
fi
